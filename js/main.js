var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
// === KONSTANTEN ===
const COLORS = ["blau", "gelb", "gruen", "orange", "rosa", "rot", "tuerkis"];
const COLORINDEX = new Map(COLORS.map((c, i) => [c, i]));
const POSITIONSINORDER = [
    { x: 1, y: 1 },
    { x: 0, y: 1 },
    { x: 1, y: 0 },
    { x: 1, y: 2 },
    { x: 2, y: 1 },
    { x: 0, y: 2 },
    { x: 2, y: 0 },
    { x: 0, y: 0 },
    { x: 2, y: 2 }
];
const NEIGHBORHOODS = [
    [{ x: 1, y: 1 }, { x: 0, y: 1 }],
    [{ x: 1, y: 1 }, { x: 1, y: 0 }],
    [{ x: 1, y: 1 }, { x: 2, y: 0 }],
    [{ x: 1, y: 1 }, { x: 2, y: 1 }],
    [{ x: 1, y: 1 }, { x: 1, y: 2 }],
    [{ x: 1, y: 1 }, { x: 0, y: 2 }],
    [{ x: 0, y: 0 }, { x: 0, y: 1 }],
    [{ x: 0, y: 1 }, { x: 0, y: 2 }],
    [{ x: 0, y: 2 }, { x: 1, y: 2 }],
    [{ x: 1, y: 2 }, { x: 2, y: 2 }],
    [{ x: 2, y: 2 }, { x: 2, y: 1 }],
    [{ x: 2, y: 1 }, { x: 2, y: 0 }],
    [{ x: 2, y: 0 }, { x: 1, y: 0 }],
    [{ x: 1, y: 0 }, { x: 0, y: 0 }],
    [{ x: 0, y: 1 }, { x: 1, y: 0 }],
    [{ x: 1, y: 2 }, { x: 2, y: 1 }],
];
// === ARBEITSPEICHER ===
const DATA = {};
const DB = {
    colorPoints: COLORS.map(c => ({ color: c, points: 0 })),
    availableColors: [],
    beete: []
};
// Alles außer das tatsächliche Lösen der Aufgabe
function start() {
    return __awaiter(this, void 0, void 0, function* () {
        let file = yield uploadFileAsync();
        extractData(file);
        console.log("Data: ", DATA);
        console.time("Laufzeit");
        let res = main();
        console.timeEnd("Laufzeit");
        let output = buildOutput(res);
        console.log(output);
        document.write(output.replace(/\n/g, "<br>").replace(/ /g, "&nbsp"), "<br><br>Öffnen Sie die Browser-Konsole (F12->Konsole) für mehr Informationen!");
    });
}
// === Lösen der Aufgabe ===
function main() {
    assignPointsToColors();
    DB.availableColors = DB.colorPoints.map(cp => cp.color).slice(0, DATA.minColors);
    const emptyBeet = [[undefined, undefined, undefined], [undefined, undefined, undefined], [undefined, undefined, undefined]];
    const emptyUsedColors = [];
    fillBestWithBest(emptyBeet, emptyUsedColors);
    let bestBeet = DB.beete.reduce((a, b) => calcTotalPoints(a) > calcTotalPoints(b) ? a : b);
    return bestBeet;
}
// === Wichtigste Subfunktionen ===
function assignPointsToColors() {
    // Für jeden Wunsch...
    for (const w of DATA.wishes) {
        // ...gib beiden Farben die Zusatzpunkte des Wunsches
        DB.colorPoints[COLORINDEX.get(w.colors[0])].points += w.points;
        DB.colorPoints[COLORINDEX.get(w.colors[1])].points += w.points;
    }
    // Sortiere alle Farben nach Punkten
    DB.colorPoints.sort((a, b) => b.points - a.points);
}
function fillBestWithBest(beet, usedColors) {
    let emptyPositions = POSITIONSINORDER.filter(p => !beet[p.x][p.y]);
    // Wenn das Beet voll ist, speichere es in der DB
    if (emptyPositions.length === 0) {
        DB.beete.push(beet);
        return;
    }
    // Finde für jede freie Position die erlaubte Farbe, die am meisten Punkte hinzugibt
    let bestAllowedColorPerPos = emptyPositions.map(pos => (Object.assign({ pos }, getBestAllowedColor(pos, beet, usedColors))));
    // Alle möglichen Schritte mit der besten Punkte-Hinzugabe finden
    let bestPoints = Math.max(...bestAllowedColorPerPos.map(c => c.points));
    let bestPossibilities = bestAllowedColorPerPos.filter(c => c.points === bestPoints);
    // Alle besten Möglichkeiten probieren
    for (const p of bestPossibilities) {
        let newBeet = copyBeet(beet);
        newBeet[p.pos.x][p.pos.y] = p.color;
        let newUsedColors = usedColors.slice();
        if (!newUsedColors.find(c => c == p.color))
            newUsedColors.push(p.color);
        fillBestWithBest(newBeet, newUsedColors);
    }
}
// === Andere Subfunktionen ===
// Wunsch bezogen
function getBestAllowedColor(pos, beet, usedColors) {
    let allowedColors = getAllowedColors(beet, usedColors);
    let neighbors = getNeighbors(pos, beet).filter(n => n);
    let bestAllowedColor = getBestColorWithNeighbors(neighbors, allowedColors);
    return bestAllowedColor;
}
function getBestColorWithNeighbors(neighbors, baseColors = COLORS) {
    let pointsPerColor = baseColors.map(c => 
    // sum of points for combinations of c with neighbors
    neighbors.map(n => getCombinationPoints(c, n)).reduce((a, b) => a + b, 0));
    let bestColorIndex = pointsPerColor.findIndex(p => p === Math.max(...pointsPerColor));
    return { color: baseColors[bestColorIndex], points: pointsPerColor[bestColorIndex] };
}
function getAllowedColors(beet, usedColors) {
    let colorsLeftToUse = DATA.minColors - usedColors.length;
    let emptyFields = beet.flat().filter(c => !c).length;
    let allowedColors = DB.availableColors;
    if (colorsLeftToUse === emptyFields)
        allowedColors = DB.availableColors.filter(c => !usedColors.includes(c));
    return allowedColors;
}
function getCombinationPoints(a, b) {
    let combination = DATA.wishes.find(w => (w.colors[0] === a && w.colors[1] === b) ||
        (w.colors[0] === b && w.colors[1] === a));
    return combination ? combination.points : 0;
}
// Beet bezogen
function calcTotalPoints(beet) {
    let points = 0;
    for (const [a, b] of NEIGHBORHOODS) {
        let colorA = beet[a.x][a.y];
        let colorB = beet[b.x][b.y];
        points += getCombinationPoints(colorA, colorB);
    }
    return points;
}
function copyBeet(beet) {
    let newBeet = [[], [], []];
    for (let x = 0; x <= 2; x++) {
        for (let y = 0; y <= 2; y++) {
            newBeet[x][y] = beet[x][y];
        }
    }
    return newBeet;
}
function getNeighbors(pos, beet) {
    return getNeighborsIndices(pos).map(pos => beet[pos.x][pos.y]);
}
function getNeighborsIndices(pos) {
    return [
        { x: pos.x + 1, y: pos.y },
        { x: pos.x - 1, y: pos.y },
        { x: pos.x, y: pos.y + 1 },
        { x: pos.x, y: pos.y - 1 },
        { x: pos.x + 1, y: pos.y - 1 },
        { x: pos.x - 1, y: pos.y + 1 }
    ].filter(pos => isPosOnField(pos));
}
function isPosOnField(pos) {
    return (pos.x >= 0 && pos.x <= 2 &&
        pos.y >= 0 && pos.y <= 2);
}
// === I/O ===
function extractData(file) {
    DATA.filename = file.name;
    let text = file.body;
    let lines = text.split(/\r?\n/g);
    DATA.minColors = Number(lines[0]);
    let wishLines = lines.slice(2, -1);
    DATA.wishes = wishLines.map(wl => {
        let strings = wl.split(" ");
        return {
            colors: [strings[0], strings[1]],
            points: Number(strings[2])
        };
    });
    DATA.wishes.sort((a, b) => a.points - b.points);
}
function buildOutput(res) {
    let points = calcTotalPoints(res);
    let NOptimalBeete = DB.beete.filter(b => calcTotalPoints(b) == points).length;
    return (`\
=== Ergebnis für ${DATA.filename} ===
Optimales Beet:
         ${res[0][0]}
      ${res[0][1]}   ${res[1][0]}
   ${res[0][2]}   ${res[1][1]}   ${res[2][0]}
      ${res[1][2]}   ${res[2][1]}
         ${res[2][2]}
Als Array (Programminterne Anordnung):
    ${res.map(c => c.join(", ")).join("\n    ")}
Punkte dafür: ${points}
Anzahl berechneter Möglichkeiten: ${DB.beete.length}
Anzahl der Lösungen mit ${points} Punkten: ${NOptimalBeete}
`);
}
//# sourceMappingURL=main.js.map