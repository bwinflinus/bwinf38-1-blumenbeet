function downloadFile(filename: string, body: string, datatype="text/plain") {
    let a = document.createElement("a");
    a.href = `data:${datatype};charset=UTF-8,${encodeURIComponent(body)}`;
    a.download = filename;
    a.click();
}

interface LoadedFile {
	name: string,
	body: string
};

function uploadFileAsync(): Promise<LoadedFile> {
	return new Promise((resolve, reject) => {
		let i = document.createElement("input");
		i.type = "file";
		i.onchange = e => {
			let file = i.files[0];
			let reader = new FileReader();
			reader.onload = r => {
				if (r.target.result) {
					let res = r.target.result;
					if (typeof res !== "string") {
						let decoder = new TextDecoder();
						res = decoder.decode(res);
					}
					resolve({name: file.name, body: res});
				} else {
					reject(`Error reading the file '${file.name}'`);
				}
			};
			reader.readAsText(file);
		}
		i.click();
	});
}
